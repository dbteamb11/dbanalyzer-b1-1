<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="globalHelper" class="com.deutsche.dba.core.ApplicationScopeHelper" scope="application"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="http://d3js.org/d3.v3.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"></link>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css"></script>
        <script src="dbanalyzer/js/main.js"></script>
        <title>Deutsche Bank Case Study</title>
    <center>
        <img src="imgaes/bigger.jpg" alt="Deustche Bank" align = "middle|top" height="250px" width="250px"/>
    </center>        
</head>

<body>
    <!--        <div id="connectionStatus">
    <%
        String dbStatus = "Status: Not Connected";
        globalHelper.setInfo("Set any value here for application level access");
        boolean connectionStatus = globalHelper.bootstrapDBConnection();

        if (connectionStatus) {
            dbStatus = "Status: Connected";
        }
    %>
    <p><%= dbStatus%></p>
    <%
        if (connectionStatus) {
    %>
</div>-->
    <div class="container" id="loginContainer" align="center">
        <h3>Welcome to DB Analyzer!</h3>
        <form action = "" id="loginForm">
            <h5><p>UserID  : <input type="text" id="f_userid" name="usr"></p> </h5>
            <h5><p>Password: <input type="password" id="f_pwd" name="pwd"></p></h5>
            <button type="button" onclick="submitUserId()" class="btn btn-primary" type="submit" >Submit</button>
            <!--input type = "submit" value = "Submit" /-->
        </form>
        <p>
        <div id="userIdMessage"></div>
    </p>
    <%
        }
    %>
</div>
<div class="container d-none" id="table">        
    <nav class="navbar sticky-top bg-light" style="margin-bottom:30px;">
        <div class="container-fluid">
            <div class="navbar-header">
                <div style="font-size:40px; color:#000080" class="navbar-brand">DB Analyzer</div>
            </div>
            <ul class="nav justify-content-center">
                <li class="nav-item btn btn-outline-primary" style="margin-left: 15px; margin-right: 5px; padding-right:30px; padding-left:30px"><a href="#" class="text-primary" style="text-decoration:none" onclick="hideTableShowGraph()">Graph</a></li>
                <li class="nav-item btn btn-outline-info" style="margin-left: 15px; margin-right: 45px; padding-right:30px; padding-left:30px"><a href="#" class="text-info" style="text-decoration:none" onclick="hideGraphShowTable()">Table</a></li>
                <li class="nav-item" style="padding-left: 30px; padding-right: 10px">
                    <select class="btn btn-secondary dropdown-toggle" id="instrument-list" onchange="displayInstrumentSpecific(this.value);">
                        <script>getAllInstruments()</script>
                    </select>
                </li>
                <li class="nav-item">
                    <select class="btn btn-secondary dropdown-toggle" id="counterparty-list" onchange="displayCounterpartySpecific(this.value);">
                        <script>getAllCounterparties()</script>
                    </select>
                </li>
            </ul>
        </div>
    </nav>
    <div id="table-graph">

    <table id="deal-list" class="table table-striped table-bordered nowrap d-none">
        <script>getDeals();</script>
    </table>
        <svg id="graph">
    </div>
</body>
</html>
