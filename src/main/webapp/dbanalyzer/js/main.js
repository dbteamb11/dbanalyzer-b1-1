var rootURL = "rws/services";

/**
 * Comment
 */
function submitUserId()
{
    submitFromForm();

}
function validateUserId()
{
    loginFromForm();
}

$(document).ready(function () {
    $("#loginForm").submit(function (event) {
        loginFromForm();
        event.preventDefault();
    });
});


function saveNewURLWithInfoFromForm() {
    $.ajax({
        method: 'POST',
        url: rootURL + '/save',
        dataType: "json", // data type of response
        data: $('form').serialize(),
        success: function () {
            $("#bookmark-success").append("<p class='text-success'>Bookmark saved successfully</p>");
            $('form').trigger("reset");  // reset the form
        }
    });
}

function hideTableShowGraph() {
    $("#deal-list").addClass('d-none');
    $("#graph").removeClass('d-none');
}

function hideGraphShowTable() {
    $("#graph").addClass('d-none');
    $("#deal-list").removeClass('d-none');
}

function loginFromForm() {
    formdata = $('form');
    fds = formdata.serialize();
    var request = $.ajax({
        method: 'POST',
        url: rootURL + '/login',
        dataType: "json", // data type of response
        data: fds,

        success: function (result) {
            setMessageUsingDOM(result);
            $("#userIdMessage").append("<p class='text-success'>You have successfully logged onto the server</p>");
            //$( 'form' ).trigger("reset");  // reset the form


        }
    });
    request.fail(function (jqXHR, textStatus, errorThrown)
    {
        alert("Request " + textStatus + ", invalid login details: " + fds);
    });
}
function submitFromForm() {

    formdata = $('form');
    fds = formdata.serialize();
    var request = $.ajax({
        method: 'POST',
        url: rootURL + '/login',
        dataType: "json", // data type of response
        data: fds,

        success: function (result) {
            setMessageUsingDOM(result);
            $("#userIdMessage").append("<p class='text-success'>You have successfully logged onto the server</p>");
            $('form').trigger("reset");  // reset the form
            $('#loginContainer').addClass("d-none");
            $('#table').removeClass("d-none");

        }
    });
    request.fail(function (jqXHR, textStatus, errorThrown)
    {
        alert("Request " + textStatus + ", invalid login details: " + fds);
    });
}

// This function is then used to update the DOM in the web page
function setMessageUsingDOM(user)
{
    var userMessageElement = document.getElementById("userIdMessage");
    var color;
    var target = $("#userIdMessage");

    if (target !== null)
    {
        target.html("<p>DEBUG JSON Representation from the server: " + JSON.stringify(user) + "</p>");
        userMessageElement.style.color = "red";
        var items = [];

        var existingElement = $("#" + user.userID);

        if (existingElement.length > 0)
            $("#" + user.userID).remove();

        //items.push( "<li id='" + user.userID + "'>" + user.userID + "->" + user.userPwd + "</li>" );

        var userList = $("<ul/>", {
            "class": "dbgrads",
            html: items.join("")
        });
        userList.appendTo("body");

        target.html("<p>JSON Representation from the server: " + JSON.stringify(user) + "</p>");
        color = "green";

        userMessageElement.style.color = color;
    } else
        target.html("<div>ERROR IN PAGE</div>");
}

function getAllCounterparties() {
    console.log("getAllCounterparties");
    $.ajax(
            {
                method: "GET",
                url: rootURL + "/counterpartyname",
                dataType: "json", // data type of response
                success: function (data)
                {
                    displayCounterparties(data);
                    console.log(data);
                    console.log("SUCCESS");
                }
            }
    );
}

function displayCounterparties(data) {
    if (data == "fail") {
        alert('error');
    } else {
        $("#counterparty-list").empty();
        $("#counterparty-list").append("<option value='all'>Select a Counterparty</option>");
        $.each(data, function (i) {
            $("#counterparty-list").append("<option value='" + data[i] + "'>" + data[i] + "</option>");
        })
    }
}

function displayCounterpartySpecific(cp) {
    if (cp === 'all') {
        getDeals();
    } else {
        $("#instrument-list").val("all");
        displaySpecific(rootURL + "/counterparties/" + cp + "/deals");
        displaySpecificMetadata(rootURL + "/deals/metadata/user/" + cp);
    }
}

function displayInstrumentSpecific(cp) {
    if (cp === 'all') {
        getDeals();
    } else {
        $('#counterparty-list').val('all');
        displaySpecific(rootURL + "/deals/" + cp);
        displaySpecificMetadata(rootURL + "/deals/metadata/instrument/" + cp);
    }
}

function displaySpecificMetadata(inputUrl) {
    console.log("displaySpecificMetadata()");
    console.log("url: " + inputUrl);
    $.ajax(
            {
                method: "GET",
                url: inputUrl,
                dataType: "json",
                success: function (data)
                {
                    plotGraph(data);
                }
            }
    );
}

function displaySpecific(inputUrl) {
    console.log("displaySpecific()");
    console.log("url: " + inputUrl);
    $.ajax(
            {
                method: "GET",
                url: inputUrl,
                dataType: "json",
                success: function (data)
                {
                    displayDeals(data);
                }
            }
    );
}

function getAllInstruments() {
    console.log("getAllInstruments");
    $.ajax(
            {
                method: "GET",
                url: rootURL + "/instrumentname",
                dataType: "json", // data type of response
                success: function (data)
                {
                    displayInstruments(data);
                    console.log(data);
                    console.log("SUCESS");
                }
            }
    );
}

function displayInstruments(data) {
    if (data == "fail") {
        alert('error');
    } else {
        $("#instrument-list").empty();
        $("#instrument-list").append("<option value='all'>Select an Instrument</option>");
        $.each(data, function (i) {
            $("#instrument-list").append("<option value='" + data[i] + "'>" + data[i] + "</option>");
        })
    }
}

function getDeals() {
    console.log("getDeals");
    $.ajax(
            {
                method: "GET",
                url: rootURL + "/deals",
                dataType: "json", // data type of response
                success: function (data)
                {
                    console.log(data)
                    console.log("data received");
                    console.log("Starting datawrite");
                    displayDeals(data);

                    console.log("SUCESS");
                }
            }
    );

}

function displayDeals(data) {
    console.log("Data size: " + data.length + "; Sample: " + data[0]);
    if (data === "fail") {
        alert('error');
    } else {
        //$('#deal-list').empty();
        var table = $('#deal-list').empty();
        table = "<thead>" + "<tr>" + "<th scope='col'>" + "Time" + "</th><th scope='col'>" + "Type" + "</th>" + "<th scope='col'>" + "Amount" + "</th><th scope='col'>" + "Quantity" + "</th>"
                + "<th scope='col'>" + "Instrument" + "</th><th scope='col'>" + "Counterparty" + "</th></tr></thead><tbody>"
        $.each(data, function (key, value) {
            table += "<tr>";
            table += "<td>" + value.time + "</td>";
            table += "<td>" + value.type + "</td>";
            table += "<td>" + value.amount + "</td>";
            table += "<td>" + value.quantity + "</td>";
            table += "<td>" + value.instrument.instrumentName + "</td>";
            table += "<td>" + value.counterparty.name + "</td>";
            table += "</tr>";
        });
        table += "</tbody>";
        $('#deal-list').append(table).DataTable();
    }

}

function plotGraph(data) {
    var margin = {
        top: 20, right: 20, bottom: 30, left: 50},
            width = 960 - margin.left - margin.right,
            height = 500 - margin.top - margin.bottom;


    var x = d3.scaleLinear().range([0, width]);
    var y = d3.scaleLinear().range([height, 0]);

    var valueline = d3.line()
            .x(function (d) {
                return x(d.id);
            })
            .y(function (d) {
                return y(d.currentPosition);
            });
    var svg = $("#graph")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                    "translate(" + margin.left + "," + margin.top + ")");
                    
    function draw(data) {
        data.forEach(function (d) {
            d.id = +d.id;
            d.currentPosition = +d.currentPosition;
        });

        x.domain(d3.extent(data, function (d) {
            return d.id;
        }));
        y.domain(d3.extent(data, function (d) {
            return d.currentPosition;
        }));


        svg.append("path")
                .data([data])
                .attr("class", "line")
                .attr("d", valueline);

        svg.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x));

// Add the Y Axis
        svg.append("g")
                .call(d3.axisLeft(y));
    }
// Get the data
    draw(data);
    }


