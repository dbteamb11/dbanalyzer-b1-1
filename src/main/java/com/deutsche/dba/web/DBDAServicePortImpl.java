/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.web;


import com.deutsche.dba.dbutils.Counterparty;
import com.deutsche.dba.dbutils.Deal;
import com.deutsche.dba.dbutils.DealHandler;
import com.deutsche.dba.tools.JsonHelper;
import com.deutsche.dba.utils.SimpleJsonMessage;
import com.deutsche.dba.core.UserController;
import com.fasterxml.jackson.core.JsonProcessingException;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


/**
 *
 * @author Selvyn
 */
@Path("/services")
public class DBDAServicePortImpl implements DBDAServicePort
{
    final   UserController userController = new UserController();

    @Override
    @GET
    @Path("/sayhello")
    public Response sayHtmlHelloTest()
    {
        String result = "<html> " + "<title>" + "DBDA" + "</title>"
                + "<body><h1>" + "the dbda is running..." + "</h1></body>" + "</html> ";

        return Response.status(200).entity(result).build();
    }
    
    @Override
    @GET
    @Path("/getAllTags")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAllTags()
    {
        String result = "getAllTags() need to be built";
    	return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @Override
    @GET
    @Path("/getAllURL")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAllURL()
    {
        String result = "getAllURL() need to be built";
    	return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
    }

    @Override
    @GET
    @Path("/get/{tags}")
    public Response getSavedURLWithInfo(@PathParam("tags") String tags)
    {
        String result = "getSavedURLWithInfo() need to be built";
    	return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
    }

    @Override
    @GET
    @Path("/login/{usr}/{pwd}")
    public Response loginWithInfo( @PathParam("url")String usr,
                                        @PathParam("description")String description,
                                        @PathParam("tags")String tags )
    {
        String result = "loginWithInfo() need to be built";
    	return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
    }

    @Override
    @POST
    @Path("/login")
    public Response loginWithInfoFromForm( @FormParam("usr") String usr,
                                        @FormParam("pwd") String pwd )
    {
        String result = userController.verifyLoginDetails(usr, pwd);
                
        if( result != null)
        {
            return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
        }
        else
            return Response.status(400).entity(new SimpleJsonMessage("fail")).build();
    }

    @GET
    @Path("/counterpartyname")
    public Response getCounterpartyList() {
        List<String> result = userController.getAllUsers();
        try {
            return Response.ok(JsonHelper.toJSON(result), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (JsonProcessingException e) {
            return Response.status(400).entity(new SimpleJsonMessage("fail")).build();
        }
    }

    @GET
    @Path("/instrumentname")
    public Response getInstrumentName() {
        List<String> result = userController.getAllInstrumentNames();
        try {
            return Response.ok(JsonHelper.toJSON(result), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (JsonProcessingException e) {
            return Response.status(400).entity(new SimpleJsonMessage("fail")).build();
        }
    }

    @GET
    @Path("/counterparties/{counterpatry}")
    public Response getCounterPartyByName(@PathParam("counterpatry") String counterPartyName) {
        Counterparty counterparty = userController.getCounterpartyByName(counterPartyName);
        try {
            return Response.ok(JsonHelper.toJSON(counterparty), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (JsonProcessingException e) {
            return Response.status(400).entity(new SimpleJsonMessage("fail")).build();
        }
    }

    @GET
    @Path("/counterparties/{counterparty}/deals")
    public Response getDealsDoneByUsers(@PathParam("counterparty") String counterPartyName) {
        List<Deal> deals = userController.getDealsByCounterpartyName(counterPartyName);
        try {
            return Response.ok(JsonHelper.toJSON(deals), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (JsonProcessingException e) {
            return Response.status(400).entity(new SimpleJsonMessage("fail")).build();
        }
    }

    @GET
    @Path("/deals/{instrument_name}")
    public Response getDealsByInstrument(@PathParam("instrument_name")String instrumentName) {
        List<Deal> deals = userController.getDealsByInstrumentName(instrumentName);
        try {
            return Response.ok(JsonHelper.toJSON(deals), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (JsonProcessingException e) {
            return Response.status(400).entity(new SimpleJsonMessage("fail")).build();
        }
    }

    @GET
    @Path("/deals")
    public Response getAllDeals() {
        List<Deal> deals = userController.getAllDeals();
        try {
            return Response.ok(JsonHelper.toJSON(deals), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (JsonProcessingException e) {
            return Response.status(400).entity(new SimpleJsonMessage("fail")).build();
        }
    }

    @GET
    @Path("/deals/metadata/user/{counterparty_name}")
    public Response getDealsPositionsByCounterPartyName(@PathParam("counterparty_name") String counterPartyName) {
        List<DealHandler.DealPosition> dealPositions = userController.getDealPositionsByCounterpartyName(counterPartyName);
        try {
            return Response.ok(JsonHelper.toJSON(dealPositions), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (JsonProcessingException e) {
            return Response.status(400).entity(new SimpleJsonMessage("fail")).build();
        }
    }

    @GET
    @Path("/deals/metadata/instrument/{instrument_name}")
    public Response getDealsPositionsByInstrumentName(@PathParam("instrument_name") String instrumentName) {
        List<DealHandler.DealPosition> dealPositions = userController.getDealPositionsByInstrumentName(instrumentName);
        try {
            return Response.ok(JsonHelper.toJSON(dealPositions), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (JsonProcessingException e) {
            return Response.status(400).entity(new SimpleJsonMessage("fail")).build();
        }
    }
}
